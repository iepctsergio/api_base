'use strict'

let express = require('express');

let api = express.Router();

let informacion = require('../controllers/informacion.controller');

api.post('/informacion', informacion.aregarInformacion);

module.exports = api;